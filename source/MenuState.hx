package;

import openfl.Assets;
import flash.geom.Rectangle;
import flash.net.SharedObject;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.util.FlxPath;
import flixel.util.FlxSave;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;

class MenuState extends FlxState {
    public var _entering:Bool = false;
    override public function create():Void {
        #if !neko
        FlxG.camera.bgColor = 0xff131c1b;
        #else
        FlxG.camera.bgColor = {rgb: 0x131c1b, a: 0xff};
        #end        
        #if !FLX_NO_MOUSE
        FlxG.mouse.show();
        #end

        var save:FlxSave = new FlxSave();
        if (save.bind("SoldierX")) {
            if (save.data.level == null) {
                // MAPA     1
                // STARS    0
                // MIN TIME 0       
                save.data.level = [
                    [0, 0], 
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, 0]
                ];
                
                ProjectClass.level = save.data.level;
            } else {
                ProjectClass.level = save.data.level;
            }

            //save.erase();
            save.close();
        }

        var title1:FlxText = new FlxText(FlxG.width / 6, FlxG.height / 4, FlxG.width, "SOLDIER-X");
        title1.size = 32;
        title1.color = 0x3a5c39;
        add(title1);

        var but:FlxText = new FlxText(0, 200, 320, "Press ENTER to die!");
        but.setFormat(null, 14, 0xff990000, "center", 0xff333333, true);
        add(but);
    }
    
    override public function destroy():Void {
        super.destroy();
    }

    override public function update():Void {
        super.update();

        if(FlxG.keys.justPressed.ENTER && !_entering) {
            FlxG.sound.play("MenuHit2");
            _entering = true;

            #if !neko
            FlxG.camera.flash(0xffd8eba2, 0.5);
            FlxG.camera.fade(0xff131c1b, 2, false, onFade);
            #else
            FlxG.camera.flash({rgb:0xd8eba2, a:0xff}, 0.5);
            FlxG.camera.fade({rgb:0x131c1b, a:0xff}, 2, false, onFade);
            #end
        }
    }

    private function onFade():Void {
        FlxG.switchState(new IntroState());
    }
}
