package;

import openfl.Assets;
import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxSave;
import flixel.tile.FlxTilemap;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectGroup;
import flixel.addons.editors.tiled.TiledTileSet;

class IntroState extends FlxState {
    private var _entering:Bool  = false;
    private var _tmx:TiledMap;
    private var _title:String;
    private var _introCount:Float = 0;
    private var _stars:Int = 0;
    private var _time:Int = 0;

    public function new():Void {
        _tmx = new TiledMap('assets/tmx/map_'+ProjectClass.levelCount+'.tmx');
        ProjectClass.tmxMap = _tmx;

        _title = _tmx.properties.resolve("title");
        _stars = ProjectClass.level[ProjectClass.levelCount-1][0];
        _time = ProjectClass.level[ProjectClass.levelCount-1][1];
        super();
    }

    override public function create():Void {
        FlxG.camera.bgColor = 0xff131c1b;
        var introText:FlxText = new FlxText(0, 100, 320, _title);
        introText.setFormat(null, 14, 0xffeeee, "center", 0xcc66000, true);
        add(introText);

        var startText:FlxText = new FlxText(0, 140, 320, "Stars: " + _stars);
        startText.setFormat(null, 14, 0xffFFD700, "center", 0xccD4AF37, true);
        add(startText);

        var startText:FlxText = new FlxText(0, 180, 320, "Time: " + _time);
        startText.setFormat(null, 14, 0xffFFD700, "center", 0xccD4AF37, true);
        add(startText);
    }

    override public function update():Void {
        super.update();

        _introCount += FlxG.elapsed;
        if ((_introCount >= 2 || FlxG.keys.justPressed.ANY) && !_entering) {
            _entering = true;

            #if !neko
            FlxG.camera.flash(0xffd8eba2, 0.5);
            FlxG.camera.fade(0xff131c1b, 1, false, onFade);
            #else
            FlxG.camera.flash({rgb:0xd8eba2, a:0xff}, 0.5);
            FlxG.camera.fade({rgb:0x131c1b, a:0xff}, 1, false, onFade);
            #end
        }
    }

    private function onFade():Void {
        FlxG.switchState(new PlayState());
    }
}
