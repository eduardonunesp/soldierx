package;

import openfl.Assets;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.util.FlxMath;
import flixel.FlxSprite;
import flixel.effects.particles.FlxEmitter;

class Bomb extends FlxSprite {
	inline static var STATE_DISABLED = 1;
	inline static var STATE_READY    = 2;
	inline static var STATE_EXPLODE  = 3;
	inline static var STATE_END      = 4;

	private var _state:Int = 1;
	private var _bombCounter:Float = 0;
	private var _bombTimeout:Int = 3;
	private var _player:Player;
	private var _destroyCounter:Float = 0;
	private var _explosion:FlxEmitter;

	private function state(state:Int):Void {
		if (state == _state) return;
		_state = state;
	}

	public function new(X:Int, Y:Int) {
		super(X, Y);
		loadGraphic("assets/gfx/bomb.png", true, true, 7);
		animation.add("3", [0]);
		animation.add("2", [1]);
		animation.add("1", [2]);
		animation.add("0", [3]);
		animation.play("3");

		var runSpeed:Int = 80;
		drag.x = runSpeed * 8;
		acceleration.y = 420;

		_player = PlayState.instance.player;
		getMidpoint(_player._point);

		offset.x = 1;
		offset.y = 1;
		width = 7;
		height = 7;

		_explosion = new FlxEmitter();
        _explosion.setXSpeed( -150, 150);
        _explosion.setYSpeed( -200, 0);
        _explosion.setRotation( -720, -720);
        _explosion.gravity = 350;
        _explosion.bounce = 0.5;
        _explosion.makeParticles(GameAssets.ImgGibs, 10, 10, true, 0.5);
        
        PlayState.instance.add(_explosion);
        PlayState.instance.hazards.add(_explosion);

        trace("Bomb created at x:"+x+" y:"+y);
	}

	override public function update():Void {
		switch (_state) {
			case STATE_DISABLED:
				disabledState();
			case STATE_READY:
				readyState();
			case STATE_EXPLODE:
				explodeState();
			case STATE_END:
				endState();
		}

		super.update();
	}

	private function disabledState():Void {
		if (FlxMath.getDistance(_point, _player._point) < 20) {
			state(STATE_READY);
		}
	}

	private function readyState():Void {
		_bombCounter += FlxG.elapsed;
		if (_bombCounter >= 1) {
			_bombCounter = 0;
			_bombTimeout--;

			if (_bombTimeout >= 0) {
				animation.play(Std.string(_bombTimeout));
				FlxG.sound.play("Countdown");
			}

			if (_bombTimeout < 0) {
				state(STATE_EXPLODE);
			}
		}
	}

	public function hitted():Void {
		kill();
		state(STATE_END);
	}

	private function explodeState():Void {
		kill();
		state(STATE_END);
	}

	private function endState():Void {
		_destroyCounter += FlxG.elapsed;
		if (_destroyCounter >= 3) {
			destroy();
		}
	}

	override public function kill():Void {
		FlxG.sound.play("Jet");
		alive = false;
		exists = false;
		_explosion.at(this);
		_explosion.start(true, 2, 0, 50);
	}
}