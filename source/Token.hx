package;

import openfl.Assets;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.FlxSprite;
import flixel.util.FlxPath;

class Token extends FlxSprite {
	public function new(X:Int, Y:Int) {
		super(X, Y);
		loadGraphic("assets/gfx/token.png", true);
		width = 6;
		height = 6;
		offset.x = 1;
		offset.y = 1;
		
		animation.add("turning",[0,1,2,3], 8);
		
		var segments:Array<FlxPoint> = new Array<FlxPoint>();
		segments.push(new FlxPoint(this.x, this.y - 4));
		segments.push(new FlxPoint(this.x, this.y + 4));
		var path:FlxPath = FlxPath.start(this, segments, 6, FlxPath.YOYO);

		animation.play("turning");
	}
	
	override public function update():Void {
		if(!alive)
			exists = false;
		else if(touching != 0)
			kill();

        super.update();
	}
	
	override public function kill():Void {
		alive = false;
		solid = false;
	}
}