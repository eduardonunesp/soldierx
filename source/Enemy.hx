package;

import Math;
import openfl.Assets;
import flixel.effects.particles.FlxEmitter;
import flixel.FlxG;
import flixel.group.FlxGroup;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxMath;
import flixel.util.FlxAngle;

class Enemy extends Soldier {	
	inline static private var STATE_WAIT:Int    = 1;
	inline static private var STATE_ATTACK:Int  = 2;
	inline static private var STATE_RUNAWAY:Int = 3;
	inline static private var STATE_DEAD:Int    = 4;

	private var _state:Int = STATE_WAIT;
	private var _player:Player;
	private var _faceCounter:Float = 0;
	private var _shotCounter:Float = 0;

	private function state(state:Int):Void {
		if (state != _state)
			_state = state;
	}

	public function new(X:Int, Y:Int, Bullets:FlxGroup, ThePlayer:Player) {
		super(X,Y, Bullets);
		setupGraphics();
		setupVars();
		_player = ThePlayer;
	}

	override public function setupGraphics():Void {
		loadGraphic("assets/gfx/enemy_1.png", true, true, 8);
		animation.add("idle", [0]);
		animation.add("run", [1, 2, 3, 0], 12);
		animation.add("jump", [4]);
		animation.add("idle_up", [5]);
		animation.add("run_up", [6, 7, 8, 5], 12);
		animation.add("jump_up", [9]);
		animation.add("jump_down", [10]);
	}

	override public function setupVars():Void {
		//bounding box tweaks
		width = 6;
		height = 7;
		offset.x = 1;
		offset.y = 1;
		
		//basic Soldier physics
		var runSpeed:Int = 80;
		drag.x = runSpeed * 8;
		acceleration.y = 420;
		_jumpPower = 200;
		maxVelocity.x = runSpeed;
		maxVelocity.y = _jumpPower;
		
		isReadyToJump = true;
	}
	
	override public function destroy():Void {
		super.destroy();
	}
	
	override public function update():Void {
		//make a little noise if you just touched the floor
		if(justTouched(FlxObject.FLOOR) && (velocity.y > 50))
			FlxG.sound.play("Land");

		switch (_state) {
			case STATE_ATTACK:
				stateAttack();
			case STATE_WAIT:
				stateWait();
			case STATE_RUNAWAY:
				stateRunAway();
			case STATE_DEAD:
				stateDead();
		}

        super.update();
	}

	private function shot():Void {
		if (_bullets != null) {
			getMidpoint(_point);
			cast(_bullets.recycle(Bullet), Bullet).shoot(_point, _aim);
		}
	}

	private function stateAttack():Void {
		if (FlxMath.getDistance(_point, _player._point) > 100) {
			if (x < _player.x) 
				facing = FlxObject.RIGHT;
			else 
				facing = FlxObject.LEFT;

			state(STATE_WAIT);
		}

		_shotCounter += FlxG.elapsed;
		if (_shotCounter >= 1) {
			_shotCounter = 0;

			var Angle:Float = FlxAngle.getAngle(_point, _player._point);

			if (Angle < -47 && Angle > -138) {
				facing = FlxObject.LEFT;
			} else if (Angle < 138 && Angle > 47) {
				facing = FlxObject.RIGHT;
			} else if (Angle > -138 && Angle < 138) {
				facing = FlxObject.UP;
			} else {
				facing = FlxObject.DOWN;
			}

			_aim = facing;
			shot();
		}
	}

	private function stateWait():Void {		
		if (FlxMath.getDistance(_point, _player._point) < 100) {
			if (x < _player.x) 
				facing = FlxObject.RIGHT;
			else 
				facing = FlxObject.LEFT;
			state(STATE_ATTACK);
		}

		_faceCounter += FlxG.elapsed;
		if (_faceCounter >= 2) {
			_faceCounter = 0;

			if (facing == FlxObject.RIGHT) 
				facing = FlxObject.LEFT;
			else 
				facing = FlxObject.RIGHT;
		}

		_aim = facing;
	}

	private function stateRunAway():Void {

	}

	private function stateDead():Void {
		
	}

	override public function kill():Void {
		if(!alive) {
			return;
		}

		solid = false;
		FlxG.sound.play("Asplode");
		FlxG.sound.play("MenuHit2");
		
		super.kill();
		exists = true;
		visible = false;

		state(STATE_DEAD);

		if(_littleGibs != null) {
			_littleGibs.at(this);
			_littleGibs.start(true, 5, 0, 50);
		}
	}
}