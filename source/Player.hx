package;

import openfl.Assets;
import flash.display.BlendMode;
import flixel.ui.FlxButton;
import flixel.effects.particles.FlxEmitter;
import flixel.FlxG;
import flixel.group.FlxGroup;
import flixel.FlxObject;
import flixel.FlxSprite;

class Player extends Soldier
{   
    private var _initPosX:Float;
    private var _initPosY:Float;
    private var _restartCounter:Float = 0;

    public function new(X:Int, Y:Int, Bullets:FlxGroup) {
        super(X,Y, Bullets);
        setupGraphics();
        setupVars();

        _initPosX = x;
        _initPosY = y;
    }

    override public function setupGraphics():Void {
        loadGraphic("assets/gfx/spaceman.png", true, true, 8);
        animation.add("idle", [0]);
        animation.add("run", [1, 2, 3, 0], 12);
        animation.add("jump", [4]);
        animation.add("idle_up", [5]);
        animation.add("run_up", [6, 7, 8, 5], 12);
        animation.add("jump_up", [9]);
        animation.add("jump_down", [10]);
    }

    override public function setupVars():Void {
        //bounding box tweaks
        width = 6;
        height = 7;
        offset.set(1, 1);
        
        //basic Soldier physics
        var runSpeed:Int = 80;
        drag.x = runSpeed * 8;
        acceleration.y = 420;
        _jumpPower = 200;
        maxVelocity.set(runSpeed, _jumpPower);
        
        isReadyToJump = true;
    }
    
    override public function destroy():Void {
        super.destroy();
    }
    
    override public function update():Void {
        if (!alive) {
            _restartCounter += FlxG.elapsed;
            if (_restartCounter > 2)
                FlxG.resetState();

            acceleration.x = 0;
            acceleration.y = 0;
            return;
        }

        //make a little noise if you just touched the floor
        if(justTouched(FlxObject.FLOOR) && (velocity.y > 50)) {
            FlxG.sound.play("Land");
        }
        
        //MOVEMENT
        acceleration.x = 0;

        #if flash
        if(FlxG.keys.pressed.LEFT || FlxG.keys.pressed.A) {
            facing = FlxObject.LEFT;
            acceleration.x -= drag.x;
        } else if(FlxG.keys.pressed.RIGHT || FlxG.keys.pressed.D) {
            facing = FlxObject.RIGHT;
            acceleration.x += drag.x;
        }

        if((FlxG.keys.justPressed.X || FlxG.keys.justPressed.SPACE) && velocity.y == 0) {
            velocity.y = -_jumpPower;
            FlxG.sound.play("Jump");
        }

        //AIMING
        if(FlxG.keys.pressed.UP || FlxG.keys.pressed.W)
            _aim = FlxObject.UP;
        else if(FlxG.keys.pressed.DOWN || FlxG.keys.pressed.S)
            _aim = FlxObject.DOWN;
        else
            _aim = facing;
        
        
        //SHOOTING
        if(FlxG.keys.justPressed.C || FlxG.mouse.justPressed) {
            if (_bullets != null) {
                getMidpoint(_point);
                cast(_bullets.recycle(Bullet), Bullet).shoot(_point, _aim);
                if(_aim == FlxObject.DOWN)
                    velocity.y -= 36;
            }
        }
        #end
        
        if (x < 5) x = 5;
        if (x > 635) x = 635;


        super.update();
    }

    override public function kill():Void {
        if(!alive)
            return;

        solid = false;
        FlxG.sound.play("Asplode");
        FlxG.sound.play("MenuHit2");
        
        super.kill();
        exists = true;
        visible = false;

        if(_littleGibs != null) {
            _littleGibs.at(this);
            _littleGibs.start(true, 5, 0, 50);
        }
    }
}
