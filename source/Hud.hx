package;

import openfl.Assets;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.FlxSprite;
import flixel.effects.particles.FlxEmitter;
import flixel.text.FlxText;
import flixel.group.FlxSpriteGroup;

class Hud extends FlxSpriteGroup {
	private var _timerText:FlxText;
	private var _timer:Int = 0;

	public function new() {
		super(0, 0);

		_timerText = new FlxText(0, 2, 320, "Time: " + _timer);
		_timerText.setFormat(null,12,0x11ffffff,"right");
		add(_timerText);
	}

	public function timePlus(time:Int):Void {
		_timer += time;
		_timerText.text = "Time: " + _timer;
	}

	public function getTimeTotal():Int {
		return _timer;
	}
}
	
