package;

import openfl.Assets;
import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxSave;
import flixel.util.FlxPoint;
import flixel.tile.FlxTilemap;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectGroup;
import flixel.addons.editors.tiled.TiledTileSet;

class EndState extends FlxState {
	private var _entering:Bool = false;
	private var _tmx:TiledMap;
	private var _timeClear:Int;
	private var _introCount:Float = 0;
	private var _stars:Int = 0;

	public function new(timeClear:Int):Void {
		super();
		_timeClear = timeClear;
		_tmx = ProjectClass.tmxMap;
	}

	override public function create():Void {
		var time_one_star:Int = Std.parseInt(_tmx.properties.resolve("time_one_star"));
		var time_two_star:Int = Std.parseInt(_tmx.properties.resolve("time_two_star"));
		var time_three_star:Int = Std.parseInt(_tmx.properties.resolve("time_three_star"));

		if (_timeClear <= time_three_star) {
			_stars = 3;
		} else if (_timeClear <= time_two_star) {
			_stars = 2;
		} else if (_timeClear <= time_one_star) {
			_stars = 1;
		} else {
			_stars = 0;
		}

		var levels:Dynamic = ProjectClass.level;
		levels[ProjectClass.levelCount-1][0] = _stars;
		levels[ProjectClass.levelCount-1][1] = _timeClear;

 		var save:FlxSave = new FlxSave();
        if (save.bind("SoldierX")) {
        	save.data.level = levels;
            save.close();
        }

		FlxG.camera.bgColor = 0xff131c1b;
		var introText:FlxText = new FlxText(0, 100, 320, "WELDONE SOLDIER");
		introText.setFormat(null, 14, 0xffeeee, "center", 0xcc66000, true);
		add(introText);

		var startText:FlxText = new FlxText(0, 140, 320, "Stars: " + _stars);
		startText.setFormat(null, 14, 0xffFFD700, "center", 0xccD4AF37, true);
		add(startText);

		var startText:FlxText = new FlxText(0, 180, 320, "Time: " + _timeClear);
		startText.setFormat(null, 14, 0xffFFD700, "center", 0xccD4AF37, true);
		add(startText);
		
		ProjectClass.levelCount++;
	}

	override public function update():Void {
		super.update();

		_introCount += FlxG.elapsed;
		if (_introCount >= 2 && !_entering) {
			_entering = true;
			FlxG.switchState(new IntroState());
		}
	}
}