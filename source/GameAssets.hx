package ;
import flash.display.Bitmap;
import flash.media.Sound;
import openfl.Assets;

class GameAssets {
	inline static public var ImgBullet:String = "assets/gfx/bullet.png";
	inline static public var ImgSpawnerGibs:String = "assets/gfx/spawner_gibs";
	inline static public var ImgSpawner:String = "assets/gfx/spawner.png";
	inline static public var ImgSpaceman:String = "assets/gfx/spaceman.png";
	inline static public var ImgTechTiles:String = "assets/gfx/tech_tiles.png";
	inline static public var ImgDirtTop:String = "assets/gfx/dirt_top.png";
	inline static public var ImgDirt:String = "assets/gfx/dirt.png";
	inline static public var ImgMiniFrame:String = "assets/gfx/miniframe.png";
	inline static public var ImgCursor:String = "assets/gfx/cursor.png";
	inline static public var ImgBloodFrag:String = "assets/gfx/blood_frag.png";
	inline static public var ImgGibs:String = "assets/gfx/gibs.png";
}