package;

import openfl.Assets;
import flixel.ui.FlxButton;
import flixel.effects.particles.FlxEmitter;
import flixel.FlxG;
import flixel.group.FlxGroup;
import flixel.FlxObject;
import flixel.FlxSprite;

class Soldier extends FlxSprite
{
    private var _aim:Int;
    private var _jumpPower:Int;
    public var isReadyToJump:Bool;
    private var _bullets:FlxGroup;
    private var _littleGibs:FlxEmitter;
    private var _explosion:FlxEmitter;
    
    public function new(X:Int, Y:Int, Bullets:FlxGroup) {
        super(X,Y);
        setupGraphics();
        setupVars();
        _bullets = Bullets;

        _explosion = new FlxEmitter();
        _explosion.setXSpeed( -150, 150);
        _explosion.setYSpeed( -200, 0);
        _explosion.setRotation( -720, -720);
        _explosion.gravity = 350;
        _explosion.bounce = 0.5;
        _explosion.makeParticles(GameAssets.ImgBloodFrag, 10, 10, true, 0.5);
        
        PlayState.instance.add(_explosion);
    }

    public function setupGraphics():Void {
        loadGraphic("assets/gfx/enemy_1.png", true, true, 8);
        animation.add("idle", [0]);
        animation.add("run", [1, 2, 3, 0], 12);
        animation.add("jump", [4]);
        animation.add("idle_up", [5]);
        animation.add("run_up", [6, 7, 8, 5], 12);
        animation.add("jump_up", [9]);
        animation.add("jump_down", [10]);
    }

    public function setupVars():Void {
        //bounding box tweaks
        width = 6;
        height = 7;
        offset.x = 1;
        offset.y = 1;
        
        //basic Soldier physics
        var runSpeed:Int = 80;
        drag.x = runSpeed * 8;
        acceleration.y = 420;
        _jumpPower = 200;
        maxVelocity.x = runSpeed;
        maxVelocity.y = _jumpPower;
        
        isReadyToJump = true;
    }
    
    override public function destroy():Void {
        super.destroy();
    }
    
    override public function update():Void {
        if(velocity.y != 0) {
            if(_aim == FlxObject.UP) animation.play("jump_up");
            else if(_aim == FlxObject.DOWN) animation.play("jump_down");
            else animation.play("jump");
        } else if(velocity.x == 0) {
            if(_aim == FlxObject.UP) animation.play("idle_up");
            else animation.play("idle");
        } else {
            if(_aim == FlxObject.UP) animation.play("run_up");
            else animation.play("run");
        }
        
        super.update();
    }

    override public function kill():Void {
        if(!alive) {
            return;
        }

        solid = false;
        FlxG.sound.play("Asplode");
        FlxG.sound.play("MenuHit2");
        
        super.kill();
        exists = true;
        visible = false;

        if(_explosion != null) {
            _explosion.at(this);
            _explosion.start(true, 5, 0, 50);
        }
    }
}