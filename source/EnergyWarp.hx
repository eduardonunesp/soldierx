package;

import openfl.Assets;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.effects.particles.FlxEmitter;

class EnergyWarp extends FlxSpriteGroup {
	private var _portalEnabled:Bool = false;

	public function enable():Void {
		if (_portalEnabled) return;

		for(obj in members) {
			obj.exists = true;
		}

		_portalEnabled = true;
	}

	public function new(X:Int, Y:Int) {
		super(X, Y);

		for (i in 0...4) {
			var sprite:FlxSprite = new FlxSprite(8*i, 0);
			sprite.loadGraphic("assets/gfx/energy_warp.png", true, true, 8);
			sprite.animation.add("on", [0,1,2,3], 10);
			sprite.animation.play("on");
			sprite.exists = false;
			sprite.solid = false;
			sprite.immovable = true;
			add(sprite);
		}

		for (i in 0...4) {
			var sprite:FlxSprite = new FlxSprite(8*i, 32);
			sprite.loadGraphic("assets/gfx/energy_warp.png", true, true, 8);
			sprite.animation.add("on", [0,1,2,3], 10);
			sprite.animation.play("on");
			sprite.facing = FlxObject.DOWN;
			sprite.exists = false;
			sprite.immovable = true;
			add(sprite);
		}
	}
}
