package;

import openfl.Assets;
import flash.geom.Rectangle;
import flash.net.SharedObject;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.group.FlxGroup;
import flixel.util.FlxPath;
import flixel.util.FlxSave;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.FlxCamera;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.tile.FlxTilemap;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectGroup;
import flixel.addons.editors.tiled.TiledTileSet;

class PlayState extends FlxState {
    private var _player:Player;
    private var _hud:Hud;
    private var _warp:EnergyWarp;
    private var _stageClear:Bool;
    private var _countStageTime:Float = 0;
    private var _tokensNeeded:Int = 0;
    private var _tokensGot:Int = 0;

    private var _tmx:TiledMap;
    private var _tileMap:FlxTilemap;
    private var _bgTileMap:FlxTilemap;
    private var _hazardMap:FlxTilemap;

    private var _soldiers:FlxGroup;
    private var _objects:FlxGroup;
    private var _enemies:FlxGroup;
    private var _bullets:FlxGroup;
    private var _enemyBullets:FlxGroup;
    private var _bombs:FlxGroup;
    private var _hazards:FlxGroup;
    private var _tokens:FlxGroup;

    public static var instance (default, null):PlayState;
    public var hazards (get, null):FlxGroup;
    private function get_hazards():FlxGroup {
        return _hazards;
    }

    public var player (get, null):Player;
    private function get_player():Player {
        return _player;
    }

	public function new() {
		_tmx = ProjectClass.tmxMap;
        _stageClear = false;
        super();
	}

	override public function create():Void {
        instance = this;
        #if !neko
        FlxG.camera.bgColor = 0xff131c1b;
        #else
        FlxG.camera.bgColor = {rgb: 0x131c1b, a: 0xff};
        #end        
        #if !FLX_NO_MOUSE
        FlxG.mouse.show();
        #end

        _bgTileMap = new FlxTilemap();
        var mapCsv:String = _tmx.getLayer('bg').csvData;
        _bgTileMap.loadMap(mapCsv, "assets/gfx/img_tiles.png", 8, 8, FlxTilemap.OFF, 1, 0, 1);
        add(_bgTileMap);
        _bgTileMap.x = 0;
        _bgTileMap.y = 0;

        _tileMap = new FlxTilemap();
        mapCsv = _tmx.getLayer('map').csvData;
        _tileMap.loadMap(mapCsv, "assets/gfx/img_tiles.png", 8, 8, FlxTilemap.OFF, 1, 0, 1);
        add(_tileMap);
        _tileMap.x = 0;
        _tileMap.y = 0;

        _hazardMap = new FlxTilemap();
        mapCsv = _tmx.getLayer('hazards').csvData;
        _hazardMap.loadMap(mapCsv, "assets/gfx/img_tiles.png", 8, 8, FlxTilemap.OFF, 1, 0, 1);
        add(_hazardMap);
        _hazardMap.x = 0;
        _hazardMap.y = 0;

        _soldiers = new FlxGroup();
        _bullets = new FlxGroup(20);
        add(_bullets);

        var playerObjects:TiledObjectGroup = _tmx.getObjectGroup("player");
        for (playerObject in playerObjects.objects) {
            if (playerObject.type == "player") {
                _player = new Player(playerObject.x, playerObject.y, _bullets);
                _soldiers.add(_player);
                add(_player);
            } else if (playerObject.type == "energy_warp") {
                _warp = new EnergyWarp(playerObject.x, playerObject.y);
                add(_warp);
            }
        }

        FlxG.camera.setBounds(0, 0, 640, 240, true);
        FlxG.camera.follow(_player, FlxCamera.STYLE_PLATFORMER);

        _hazards = new FlxGroup();
        _enemies = new FlxGroup();
        _bombs = new FlxGroup();
        _tokens = new FlxGroup();
        _hazards.add(_bombs);
        add(_enemies);
        add(_bombs);
        add(_tokens);

        _enemyBullets = new FlxGroup(20);
        add(_enemyBullets);

        _objects = new FlxGroup();
        _objects.add(_player);
        _objects.add(_bullets);
        _objects.add(_enemyBullets);
        _objects.add(_enemies);
        _objects.add(_hazards);

        var enemyObjects:TiledObjectGroup = _tmx.getObjectGroup("enemies");
        for (enemyObject in enemyObjects.objects) {
            var e:Enemy = new Enemy(enemyObject.x, enemyObject.y, _enemyBullets, _player);
            _enemies.add(e);
            _soldiers.add(e);
        }

        var bombObjects:TiledObjectGroup = _tmx.getObjectGroup("bombs");
        for (bombObject in bombObjects.objects) {
            _bombs.add(new Bomb(bombObject.x, bombObject.y));
        }

        var tokensObjects:TiledObjectGroup = _tmx.getObjectGroup("tokens");
        for (tokenObject in tokensObjects.objects) {
            _tokens.add(new Token(tokenObject.x, tokenObject.y));
            _tokensNeeded++;
        }

        FlxG.sound.playMusic("Theme1", 0.10);

        _hud = new Hud();
        add(_hud);

        _hud.setAll("scrollFactor", new FlxPoint(0, 0));
        _hud.setAll("cameras", [FlxG.camera]);
	}

    override public function destroy():Void {
        super.destroy();
    }

    override public function update():Void {
        FlxG.collide(_tileMap, _objects);
        FlxG.collide(_hazardMap, _soldiers, hazard_map_collide);
        FlxG.overlap(_player, _tokens, collect);
        FlxG.overlap(_player, _hazards, hurts);
        FlxG.overlap(_player, _enemyBullets, hurts);
        FlxG.overlap(_bullets, _hazards, bullet_hit);
        FlxG.overlap(_bullets, _enemies, bullet_hit);
        FlxG.overlap(_enemies, _hazards, hurts);
        FlxG.overlap(_enemyBullets, _hazards, enemy_bullet_hit);
        FlxG.collide(_player, _warp, warping);

        if (_tokensGot >= _tokensNeeded)
            _warp.enable();

        _countStageTime += FlxG.elapsed;
        if (_countStageTime >= 1) {
            _countStageTime = 0;

            if (!_stageClear)
                _hud.timePlus(1);
        }

        super.update();
    }

    private function hazard_map_collide(Object1:FlxObject, Object2:FlxObject):Void {
        Object2.kill();
    }

    private function collect(Object1:FlxObject, Object2:FlxObject):Void {
        Object2.kill();
        FlxG.sound.play("Beep");
        _tokensGot++;
    }

    private function hurts(Object1:FlxObject, Object2:FlxObject):Void {
        if (Std.is(Object2, Bomb)) return;
            Object1.hurt(1);
    }

    private function bullet_hit(Object1:FlxObject, Object2:FlxObject):Void {
        Object1.kill();
        Object2.hurt(1);
    }

    private function enemy_bullet_hit(Object1:FlxObject, Object2:FlxObject):Void {
        Object1.kill();
        Object2.hurt(1);
    }

    private function warping(Object1:FlxObject, Object2:FlxObject):Void {
        if (_stageClear) return;
        _stageClear = true;
   
        #if !neko
        FlxG.camera.flash(0xffd8eba2, 0.5);
        FlxG.camera.fade(0xff131c1b, 1, false, onFade);
        #else
        FlxG.camera.flash({rgb:0xd8eba2, a:0xff}, 0.5);
        FlxG.camera.fade({rgb:0x131c1b, a:0xff}, 1, false, onFade);
        #end
    }

    
    private function onFade():Void {
        FlxG.switchState(new EndState(_hud.getTimeTotal()));
    }
}