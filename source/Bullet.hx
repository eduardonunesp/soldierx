package;

import openfl.Assets;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.FlxSprite;

#if (cpp || neko)
import flixel.atlas.FlxAtlas;
#end

class Bullet extends FlxSprite {
	public var speed:Float;
	
	public function new() {
		super();
		loadGraphic("assets/gfx/bullet.png", true);
		width = 6;
		height = 6;
		offset.x = 1;
		offset.y = 1;
		
		animation.add("up",[0]);
		animation.add("down",[1]);
		animation.add("left",[2]);
		animation.add("right",[3]);
		animation.add("poof",[4, 5, 6, 7], 50, false);
		
		speed = 360;
		#if (cpp || neko)
		//atlas = new FlxAtlas
		#end
	}
	
	override public function update():Void {
		if(!alive) {
			exists = false;
		} else if(touching != 0) {
			kill();
		}

        super.update();
	}
	
	override public function kill():Void {
		if(!alive) {return;}
		velocity.x = 0;
		velocity.y = 0;
		if(onScreen()) {FlxG.sound.play("Jump");}
		alive = false;
		solid = false;
		animation.play("poof");
	}
	
	public function shoot(Location:FlxPoint, Aim:Int):Void {
		FlxG.sound.play("Shoot");
		
		super.reset(Location.x - width / 2, Location.y - height / 2);
		solid = true;
		switch(Aim) {
			case FlxObject.UP:
				animation.play("up");
				velocity.y = -speed;
			case FlxObject.DOWN:
				animation.play("down");
				velocity.y = speed;
			case FlxObject.LEFT:
				animation.play("left");
				velocity.x = -speed;
			case FlxObject.RIGHT:
				animation.play("right");
				velocity.x = speed;
		}
	}
}